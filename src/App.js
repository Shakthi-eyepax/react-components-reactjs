import React from 'react';
import './App.css';
import ChildComponent from './ChildComponent/ChildComponent';

/* function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
} */

class App extends React.Component {
  constructor(props) {
    console.log('Parent Constructor called')
    super();
    this.state = {
      count: 0,
      initialValue: 400,
      bid: 0,
      showPercentage: false,
    }
    console.log(this.state)
  }

  //best place to API calls
  componentDidMount = () => {
    console.log('App componentDidMount is being called')
  }

  componentWillUnmount = () => {
    console.log('App componentWillUnmount is being called')
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('App componentWillReceiveProps is being called')
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    console.log('App shouldComponentUpdate is being called')
    console.log(this.state)
    console.log(nextState)
    return true;
  }

  togglePercentage = () => {
    this.setState({ showPercentage: !this.state.showPercentage })
  }

  increaseCount = () => {
    this.setState({ count: this.state.count + 1 })
  }

  onBidChanged = (e) => {
    let val = e.target.value
    if (val.match('^[0-9]*$')) {
      this.setState({ bid: Number(val) })
    }
  }

  clearBid = () => {
    this.setState({ bid: 0 })
  }

  render() {
    console.log('Parent Render called')
    return (
      <div className='App-container'>
        <div>
          <h5>App Component</h5>
          <span>Count {this.state.count}</span>
          <br />
          <button onClick={this.increaseCount}>Increase</button>
          <br />
          <br />
          <span>Initial Value: LKR {this.state.initialValue}</span>
          <br />
          <span>Bid: </span>
          <input type='number' onChange={this.onBidChanged}
            value={this.state.bid} />
          <br />
          <button className='toggle-button'
            onClick={this.togglePercentage}>
            {this.state.showPercentage ? 'Show value' : 'Show percentage'}
          </button>
        </div>
        <ChildComponent showPercentage={this.state.showPercentage}
          initialValue={this.state.initialValue}
          bid={this.state.bid}
          clearBid={this.clearBid} />
      </div>
    )
  }
}

export default App
