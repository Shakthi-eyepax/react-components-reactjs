import React from "react"
import './ChildComponent.css'

class ChildComponent extends React.Component {
    state = {
        text: '',
        bid: 0
    }

    componentDidMount = () => {
        console.log('Child componentDidMount is being called')
    }

    componentWillUnmount = () => {
        console.log('Child componentWillUnmount is being called')
    }

    componentWillReceiveProps = (nextProps) => {
        console.log('Child componentWillReceiveProps is being called')
        if (nextProps.showPercentage !== this.props.showPercentage)
            this.setState({ text: '' })
        if (nextProps.bid !== this.props.bid)
            this.setState({ bid: nextProps.bid })
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        console.log('Child shouldComponentUpdate is being called')
        if (this.props.showPercentage !== nextProps.showPercentage ||
            this.props.bid !== nextProps.bid)
            return true;
        else return false;
    }

    render() {
        console.log('Child Render called')
        const { showPercentage, initialValue, bid, clearBid } = this.props
        const value = bid - initialValue
        const percent = value / initialValue * 100
        return (
            <div className='Child-container'>
                <h5>Child Component</h5>
                <p>Show Percentage {showPercentage ? `Percentage ${percent}%` : `Value LKR ${value}`}</p>
                <button className='toggle-button'
                    disabled={bid === 0}
                    onClick={clearBid}>Clear Bid
                </button>
            </div>
        )
    }
}

export default ChildComponent
